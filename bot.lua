local mobdebug = nil--require("mobdebug")
if mobdebug then mobdebug.start() end

local livit = require "./main.lua"
local pp = require "pretty-print"
local rin = require "coro-channel".wrapStream(pp.stdin)

local jit = require "jit"

local NICK = "livit"
local newline = "\x10n"

local X = "Ｘ"
local O = "Ｏ"
local E = "　"

local function ctcp_game(game)
  return newline ..
  game[1] .. " " .. game[2] .. " " .. game[3] .. newline ..
  game[4] .. " " .. game[5] .. " " .. game[6] .. newline ..
  game[7] .. " " .. game[8] .. " " .. game[9]
end

coroutine.wrap(function()
    if mobdebug then mobdebug.on() end
    local _r, _w = livit.connect "irc.freenode.net" (NICK)

    local print_reader_helper = function(...) print((...).raw) return ... end
    local print_reader = function(...) return print_reader_helper(_r(...)) end
    local print_writer = function(...) print(...) return _w(...) end

    local r, w = livit.ping(print_reader, print_writer)

    coroutine.wrap(function()
        if mobdebug then mobdebug.on() end
        local flag = true
        local games = {}
        while true do
          local v = r()
          if v.command == 005 and flag then
            w("JOIN #ctcp-s-toes")
            flag = false
          end
          if v.command == "PRIVMSG" then
            if v[1] == "#ctcp-s-toes" then
              local srcnick = livit.lower(v.source:match("^:([^! ]+)!"))
              local msg = v[2]:match("^:?(.*)$")
              local newgame = msg:match("^.new ([A-}][0-9A-}-]*)")
              if newgame then
                newgame = livit.lower(newgame)
                if not games[srcnick] then games[srcnick] = {} end
                if not games[newgame] then games[newgame] = {} end
                if not (games[srcnick][newgame] and games[newgame][srcnick]) then
                  local game = games[newgame][srcnick]
                  local accept = false
                  if not game then
                    game = {
                      E, E, E,
                      E, E, E,
                      E, E, E,
                    }
                    games[newgame][srcnick] = false
                    print("Starting new game between " .. srcnick .. " and " .. newgame)
                  else
                    accept = true
                    print("New game between " .. newgame .. " and " .. srcnick .. " started")
                  end
                  games[srcnick][newgame] = game

                  if accept then
                    game.turn = newgame
                    game.key = {[newgame] = X, [srcnick] = O}
                    w("PRIVMSG #ctcp-s-toes :New game between " .. newgame ..
                      " and " .. srcnick .. " started! " .. newgame .. ", it's your turn!")
                    w("PRIVMSG #ctcp-s-toes :" .. ctcp_game(game))
                    -- TODO?
                  end
                end
              end
              local target, cell = msg:match("^" .. NICK .. ": ([A-}][0-9A-}-]*) ([1-9])$")
              if target then
                target = livit.lower(target)
                if srcnick == target then
                  w("PRIVMSG #ctcp-s-toes :" .. srcnick .. ", please wait for your turn.")
                elseif games[srcnick] and games[srcnick][target] then
                  local game = games[srcnick][target]
                  cell = tonumber(cell)
                  if game.turn == srcnick and game[cell] == E then
                    game[cell] = game.key[srcnick]
                    game.turn = target
                    w("PRIVMSG #ctcp-s-toes :" .. ctcp_game(game) .. newline ..
                      game.turn .. ", it's your turn!")
                    local x = (cell - 1) % 3 + 1
                    local y = math.ceil(cell / 3) * 3
                    local key = game.key[srcnick]
                    if (game[y - 2] == key and game[y - 1] == key and game[y] == key)
                    or (game[x] == key and game[3+x] == key and game[6+x] == key)
                    or (game[1] == key and game[5] == key and game[9] == key)
                    or (game[3] == key and game[5] == key and game[7] == key) then
                      w("PRIVMSG #ctcp-s-toes :Congratulations " .. srcnick .. ", you won the game!")
                      games[srcnick][target] = nil
                      games[target][srcnick] = nil
                    end
                  end
                end
              end
            end
          elseif v.command == "QUIT" or v.command == "PART" then
            local srcnick = livit.lower(v.source:match("^:([^! ]+)!"))
            if games[srcnick] then
              for k,v in pairs(games[srcnick]) do
                games[k][srcnick] = nil
              end
              games[srcnick] = nil
            end
          elseif v.command == "NICK" then
            local newnick = livit.lower(v[1]:match("^:?(.*)"))
            local srcnick = livit.lower(v.source:match("^:([^! ]+)!"))
            if games[srcnick] then
              for k,v in pairs(games[srcnick]) do
                games[k][newnick] = games[k][srcnick]
                games[k][srcnick] = nil
              end
              games[newnick] = games[srcnick]
              games[srcnick] = nil
            end
          end
        end
      end)()
    coroutine.wrap(function()
        if mobdebug then mobdebug.on() end
        while true do
          local v = rin()
          if v:sub(1, 4) == "JIT " then
            if v:sub(5) == "ON" then
              jit.on()
              print("JIT enabled")
            elseif v:sub(5) == "OFF" then
              jit.off()
              jit.flush()
              print("JIT disabled")
            end
          else
            _w(v)
          end
        end
      end)()
  end)()
