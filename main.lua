local coronet = require 'coro-net'
local corowrapper= require 'coro-wrapper'

local type = type
local ipairs = ipairs
local tonumber = tonumber
local tconcat = table.concat
local gsub = string.gsub
local schar, sbyte = string.char, string.byte

-- Helper functions

-- Scans chunks for \n
local function scanLine(chunk)
  return chunk:find('\n', 1, true)
end

-- Splits lines at \r\n
local function decodeLine(chunk, index)
  local v, i = chunk:match("^(.-)\r\n()", index)
  if #chunk == i then return v, nil end
  return v, i
end

-- Appends \r\n
local function encodeObject(o, w)
  if w then -- future-proof
    if type(o) == "table" then
      for i,v in ipairs(o) do
        w(v) w('\r\n')
      end
    else
      w(o) w('\r\n')
    end
  else
    if type(o) == "table" then
      return tconcat(o, '\r\n') .. '\r\n'
    else
      return o .. '\r\n'
    end
  end
end

-- Parses lines into a table
local function tableLine(chunk, index)
  local t = {raw=chunk:sub(index)}
  local i = index
  if chunk:sub(i,i) == "@" then
    -- IRCv3 garbage
    t.ircv3_tags, i = chunk:match("^ ?([^ ]+) ?()", i)
  end
  local source = ""
  if chunk:sub(i,i) == ":" then
    -- source
    t.source, i = chunk:match("^ ?([^ ]+) ?()", i)
  end
  t.command, i = chunk:match("^ ?([^ ]+) ?()", i) -- command
  if t.command:find("^[0123456789][0123456789][0123456789]$") then -- numeric
    t.command = tonumber(t.command)
  end
  local x = 0
  while true do
    x = x + 1
    local arg, ni = chunk:match("^ ?([^ ]+) ?()", i)
    if not arg then break end
    if arg:sub(1,1) == ":" then
      t[x] = chunk:sub(i)
      break
    end
    t[x], i = arg, ni
  end
  return t
end

-- Opens a connection.
local function connect(HOST, PORT, TLS)
  PORT = PORT or 6667
  local r, w = coronet.connect {
    host = HOST,
    port = PORT,
    tls = TLS
  }

  r = corowrapper.merger(r, scanLine)
  r = corowrapper.decoder(r, decodeLine)
  r = corowrapper.decoder(r, tableLine)
  w = corowrapper.encoder(w, encodeObject)

  return function(NICK, USER, NAME, PASS)
    USER = USER or NICK
    NAME = NAME or USER
    if PASS then
      w('PASS ' .. PASS)
    end
    w('NICK ' .. NICK)
    w('USER ' .. USER .. ' * * :' .. NAME)
    return r, w
  end, r, w
end

-- Creates a read function that automatically replies to pings
local function ping(r, w)
  return function()
    while true do
      local item = r()
      if item.command == "PING" then
        w("PONG " .. tconcat(item, ' '))
      end
      return item
    end
  end, w
end

-- IRC compliant lowercase function
local function lower(str)
  return gsub(str, "[A-Z%[%\\%]]", function(c) return schar(sbyte(c)+32) end)
end

-- IRC compliant uppercase function
local function upper(str)
  return gsub(str, "[a-z%{%|%}]", function(c) return schar(sbyte(c)-32) end)
end

return {connect = connect, ping = ping, lower = lower, upper = upper}